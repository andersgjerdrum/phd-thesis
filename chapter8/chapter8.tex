Distributed systems are favorable in situations which require scale, availability and fault tolerance. 
However, these systems require bespoke design decisions for security where distributing state implies distributing secrets to multiple physical locations. 
Distribution increases the attack surface (\ac{tcb}) of a software system, and complex inter-process relationships may moreover lead to bugs which are traditionally hard to discover.

Diggi is designed and implemented to enable the development of secure distributed services hosted in a untrusted public cloud.
The work presented in this thesis is in part influenced by, and topically situated, among a tapestry of related research.
This chapter presents a selection of the most relevant concepts and systems which protect hosted applications from an untrusted underlying system.

\section{Mitigating and improving \ac{sgx}-based systems}
Native code executing in enclave memory is exposed to bugs or exploits which trigger data-leaks to the writable host process' memory address space.
Systems which instrument memory references~\cite{orenbach2019cosmix} or implement memory safety as part of a natively compiled language~\cite{wang2019towards} are able to protect against ROP-style attacks or stray pointers triggering unsolicited writes to untrusted memory.
Similarly, \textcite{kuvaiskii2017sgxbounds} implement memory safety in \ac{sgx} using tagged memory pointers, which hide allocation metadata in the unused upper 32 bits of each enclave memory reference.

Diggi supports legacy systems, by exposing a translator server abstraction to simplify partitioning and porting existing systems into \ac{sgx}.
Similar efforts simplify development by integrating enclave components as a programming language extensions which minimize the porting work necessary to partition applications into trusted and untrusted components~\cite{ghosn2019secured}.

\textcite{orenbach2020autarky} conceal the controlled side-channel in the \ac{sgx} memory model by proposing a set of modifications to the \ac{sgx} ISA which gives an enclave exclusive control over its own page faults. 

Rollback attacks may return an application to a known good prior authentic state in non-ideempotent systems triggering behaviour such as  \emph{double spending}. 
Forking attacks are implemented by selectively rolling back state to different clients for a given application. 
Diggi solves this problem by implementing accountable cloud functions, where the burden of a provable interaction is on the cloud function to submit evidence back to the client.
\textcite{brandenburger2017rollback} introduce \emph{Lightweight Collective Memory}, a distributed protocol to detect integrity and consistency violations among multiple participating system clients, for operations with side-effects. 

Diggi is built from the ground up to be efficient, adhering to the performance principles outlined in Section \ref{section:principles}. 
A large part of this work, revolves around managing enclave memory usage and context switch frequency.
The memory integrity protection features for \ac{sgx} incurs a significant overhead and \textcite{taassori2018vault} addresses the overhead by simulating an alternative organization of the \ac{epc} into a variable arity unified tree~(VAULT).
Similar to our findings, \textcite{weisse2017regaining} realize that context switches are expensive and implement HotCalls. 
Through a spin-lock based mechanism, dedicated threads within and outside are able to conduct system calls in synchrony, exhibiting close to native performance.

Cloud services require elasticity to be efficient, and migrating runtimes is therefore a requirement for capacity management.
As discussed in Chapter \ref{chapter:tee}, \ac{sgx} enclaves use ephemeral keys to encrypt both memory and sealed state, uniquely tied to the hardware platform instance.
Additionally, migration of trusted enclaves is susceptible to fork and rollback attacks by the untrusted cloud.
\textcite{gu2017secure} propose a secure protocol for live migration of \ac{sgx} enclaves.
 \textcite{alder2018migrating} extend this, by also supporting architectural services such as monotonic counters and sealed data.

Intel \ac{sgx} is an inherently proprietary system, several aspects of which are confidential.
Systems which interact with \ac{ias} to attest production enclave signatures are required to be explicitly onboarded onto the \ac{sgx} ecosystem.
Although Intel supports developer provisioned attestation keys, the process still involves approval, and the reliance on, proprietary undocumented technology.
\textcite{chen2019opera} propose OPERA, an open \ac{sgx} attestation service decoupled from Intels \ac{ias}. 
OPERA is a standalone, open source attestation service providing the same privacy-preserving and verifiable properties as \ac{ias}, without relying on a single point of failure.

\textcite{costan2016intel} conduct a comprehensive security analysis of
SGX, falsifying some of its guarantees by explaining in detail
exploitable vulnerabilities within the architecture. 
These lead the authors to implement Sanctum~\cite{costan2016sanctum},
an alternative hardware architectural extension
providing many of the same properties as \ac{sgx}, but targeted
towards the Rocket RISC-V chip architecture. 

\section{Formal Methods, Verifiable Execution and Policy Enforcement}
As described above, despite the benefit of trusted execution, bugs, misconfigurations or system level attacks such as rollback or denial of service, require additional measures to protect from an untrusted environment.

Moat~\cite{Sinha:2015:MVC:2810103.2813608} is a tool which utilizes formal verification to ensure the confidentiality of code executing within enclaves. 
Based on precise adversarial models and formal specification of \ac{sgx} system properties, Moat uses information flow analysis and automatic theorem evaluation to prove the confidentiality of trusted execution.

%formal foundations, a "model" for TEEs
\textcite{subramanyan2017formal} introduce the concept of a trusted abstract platform (TAP); a formal description of an ideal enclave system with a parametrizable adversarial model. 
TAP presents machine checkable proofs showing that the platform is confidential, integrity preserving and provides secure remote attestation. 
Moreover, the authors additionally demonstrate how existing enclave systems such as \ac{sgx} and Sanctum implement secure enclaves under the respective adversarial models.
%replacing sgx with formal methods.

\textcite{ferraiuolo2017komodo} acknowledge the extreme complexity of the \ac{sgx} microarchitecture and its limited potential to mitigate design flaws and bugs. 
Komodo decouples trusted hardware and securely hosted software through a privileged software \emph{monitor}. 
The correctness and security properties are similarly machine-checkable through formalized proofs.

Delegated enforcement through trustable software constructs, mediates both access latency for policy verification, and increases the potential richness of remotely enforceable policies.
\textcite{Birrell:2018:SEU:3267323.3268954} evaluate multiple architectural designs for implementing use-based policy enforcement using Intel \ac{sgx}. 
Software attestation enables reference monitor designs outsourcing enforcement traditionally only executed at source. 
By moving logic closer to data via attested enclaves, the overhead of policy enforcement is reduced.
This work is implemented on an early version of Diggi.
\textcite{krahn2018pesos} recognize a similar opportunity, and enable rich policy enforcement on storage separate from the I/O stack by using \ac{sgx}. 
Policy Enhanced Secure Object Store (PESOS) may host policy compliant data on untrusted systems.
\textcite{matetic2018delegatee} introduce \emph{brokered delegation}, which allow flexible delegation of credential and access rights to third party systems, protected by \acp{tee}.
\textcite{djoko2019nexus} use \ac{sgx} to implement credential based access controll of storage resources protected by \ac{sgx}. 
Nexus avoids expensive re-encryption of storage as all access to physical volumes occur through enclaves.


PeerReview \cite{haeberlen2007peerreview} implements accountability in distributed systems, where Byzantine failures are detectable and linked to faults. 
Moreover, a correct node may prove itself against false accusations. 
PeerReview implements support for these properties through a secure log of all messages transmitted by each node in the system. 
PeerReview requires a correct node to be deterministic, sign messages and periodically audit the system.
SGX-Log~\cite{karande2017sgx} implements tamperproof and encrypted storage of system logs for use in an audit procedure, such as forensic analysis.
Similarly, CUSTOS~\cite{paccagnella2020custos} is a framework implementing tamper evident audit procedures of Operating Systems using \acp{tee}.
CUSTOS additionally implements a decentralized audit protocol, capable of real-time detection of integrity violations. 
This approach bares similarity to Lightweight Collective Memory~\cite{brandenburger2017rollback}, discussed in the previous section.
LibSeal~\cite{aublin2018libseal} implements a non-repudable service log  which is able to arbit claims for SLA violations between a service provider and clients, through \acp{tee}. 
LibSeal interposes a TLS library mediating all network access, logging all interaction to sealed storage.

\section{Secure Analytics and Storage systems}
Securing data processing pipelines and storage from an untrusted cloud is made possible through progress in hardware design, encryption techniques and algorithm research. 
For \ac{sgx} specifically, hosting large volumes of data in enclave memory is infeasible due to the expected overhead. 
Data replay attacks may attempt to repeat stale information to hosted enclaves.
Additionally, data-dependent operations on sensitive data will inherently leak information to the untrusted system.

EnclaveDB~\cite{priebe2018enclavedb} partitions the Microsoft Hekaton in-memory SQL Server by moving parts of the query processing into trusted memory. 
EnclaveDB mitigates replay attacks by reusing transaction identifiers as nonces. 
The in-memory Hekaton engine stores all memory slabs in \ac{prm} and is, due to memory restrictions in current \ac{sgx} hardware, only evaluated at scale on a simulated test bench by injecting speculative performance penalty timings into the executable binary. 
EnclaveDB briefly mentions hosting mutually distrusting database instances within the Hekaton engine, however does not describe the concept further.
%The total TCB for Hekaton components running within the secure enclave environment is roughly 384K lines of C/C++.
TrustedDB~\cite{bajaj2014trusteddb} and Cipherbase~\cite{arasu2013orthogonal} support queries on encrypted data using trusted hardware, however does not provide complete confidentiality or integrity protection. 
Furthermore, the two are limited to custom hardware co-processors, not generally available on commodity platforms.
\textcite{sartakov2018stanlite} implement a secure rack-scale in-memory database using \ac{sgx} and SQLite. 
Similarly to Diggi ephemeral storage, STANLite uses user-level paging to efficiently use regular DRAM for data storage rather than the restrictive pool of \ac{epc}.
Additionally, STANLite uses \ac{rdma} to distribute storage across a rack of \ac{sgx} capable systems.
\textcite{weiser2017sgxio} proposes a generic I/O architecture for creating trusted paths connecting generic peripheral devices securely to enclave software, without inspection from the hypervisor or host \ac{os}.

More complex processing and lookup techniques require additional care to not expose data-dependent operations to the host system.
ObliDB~\cite{eskandarian2019oblidb} implements generic analytical query processing for \ac{sgx}-capable \acp{dbms}, protecting execution from data-dependent processing.
Similarly, Oblix~\cite{mishra2018oblix} implements an oblivious search index structure for protecting server index operations and responses from clients by doubly-oblivious data-structures. These data-structures protect both client interrogation and server interrogation from inferring data-dependent access patterns.
%system, query processor, obfuscation, using OR operator.
X-Search~\cite{mokhtar2017x} implements a private search engine proxy, hosted in an software attested enclave.
Queries are obfuscated via a novel algorithm which packs k random past queriers, ORs them together, before submitting them to the search engine endpoint.
\textcite{pires2018cyclosa} similarly solves private web-search through a \ac{sgx}-based client browser extension.

\textcite{ahmad2018obliviate} implement a data oblivious filesystem by filtering read and write operations to the host system through an ORAM protocol.
 A similar approach may be implemented to shield against data-dependent access for select system call translations and ephemeral storage in Diggi. 

Similarly to storage, analysis of data may also expose information through data-dependent processing.
VC3~\textcite{schuster2015vc3} introduce a distributed map-reduce system for trusted analytics in the cloud using \ac{sgx}. 
VC3 runs on unmodified Hadoop, however ensures that it, the operating system and hypervisor is held outside of the TCB.
\textcite{zheng2017opaque} implement a distributed oblivious analytics platform based on Apache Spark SQL. 
Opaque uses \ac{sgx} as an optimized memory cache to speed up oblivious memory operations. 
A trade-of between generic implementation and performance leads Opaque to implement oblivious relational queries through modifications to the Catalyst query optimizer. 
Several distributed oblivious relational operations are proposed, solving a broad category of filtering and data aggregation tasks.

\textcite{Ohrimenko2016} implement secure multi-party machine learning. 
By storing datasets in enclaves, two untrusted parties may share data for a joint machine learning task without sharing source data. 
Trained models may then be downloaded for use into \ac{sgx} enclaves.
To avoid side-channels they introduce \emph{data obliviousness} which constructs  data independent machine learning algorithms for SVM, matrix factorization, neural networks, decision trees and k-means clustering. 
We consider this work complementary to Diggi, and other prior work have demonstrated the practical application of effective general countermeasures against memory dependent side-channel attacks\cite{shih2017t}. 
This work does not detail the differential privacy concerns of the joint trained model nor the revocation procedure for guaranteed erasure once learning is complete. 
\textcite{kuccuk2016exploring} apply this concept similarly by utilizing \ac{sgx} to solve privacy preserving multi-party energy metering.

Statistical inference on sensitive datasets will inherently contain traces of the original data. 
Differential privacy is therefore very hard to ensure in trained models.
Each query towards a private model will leak some small delta of information.
\textcite{song2017machine} evaluate the concept of "memorizing" a trained model, through blackbox access, and demonstrate that the resulting model can again demonstrate high predictive power.
\textcite{mo2020darknetz} similarly recognize this problem and evaluate a framework for protecting the privacy of trained Deep Neural Network~(DNN)-models on TrustZone-capable edge devices. 

\section{Trusted runtimes in TEEs}
The Diggi trusted runtime is designed from the ground up to support efficient execution of cloud functions in \ac{sgx} with a small \ac{tcb}. 
Additionally, Diggi implements support for legacy features by allowing selective implementation of external system translators. 
Table \ref{fig:tcb_loc} compares the \ac{tcb} of Diggi as measured by \ac{sloc}\footnote{https://dwheeler.com/sloccount/} to that of  noteworthy similar trusted runtimes in \ac{sgx}.

\textcite{baumann2015shielding} were the first to explore legacy applications executing inside \ac{sgx}. 
By modifying a library \ac{os} implementation of Windows 8~\cite{porter2011rethinking}, Haven is able to host unmodified applications entirely inside an \ac{sgx} enclave. 
The Haven \ac{tcb} comprises some 5 million \ac{sloc} executing inside the enclave, exposing a considerable attack surface. 
Haven implements block encryption of IO, and stores nonces for replay protection separately, using Merkle-trees for integrity-protection of data. 
This work predates the general availability of \ac{sgx} and all experimental evaluations are therefore conducted on a proprietary emulator provided by Intel. 

Ryoan~\cite{hunt2018ryoan} implements a distributed
sandbox facilitating untrusted computing on secret data
residing on third-party cloud services. Ryoan proposes a new
request oriented data-model where processing modules are
activated once without persisting data input to them.
Ryoan creates a shielding construct supporting mutual distrust
between the application and the infrastructure by
combining sandboxing techniques with \ac{sgx}. 
Through remote attestation, Ryoan is able to verify the
integrity of sandbox instances and protect execution. 
As Haven, Ryoan predates availability of \ac{sgx}, and large parts of its
evaluation is conducted in an SGX emulator based on QEMU.
A similar effort by \textcite{goltzsche2019acctee} implements this concept in a 2-way sandbox to solve resource accounting between two mutually distrusting parties.

\textcite{arnautov2016scone} design a \ac{sgx}-capable container runtime by investigating the tradeoff between multiple designs supporting the execution of unmodified legacy applications inside of \ac{sgx}. 
Three different design configurations for the partitioning of the application stack between the trusted and untrusted environment are evaluated; a library \ac{os}, a standard application library, and a minimal stub interface. 
Each choice holds several performance and security tradeoffs. 
More importantly, this work demonstrates the performance gain of implementing exit-less user-level threading and system calls, an approach which has inspired the design of Diggi.
\textcite{vaucher2018sgx} further expand on \ac{sgx}-capable containers and describe the design and implementation of support inside the Kubernetes container orchestrator.

\textcite{tsai2017graphene} implement support for unmodified applications executing in SGX by modifying the Graphene library \ac{os}~\cite{tsai2014cooperation}. 
This work includes support for multiprocessing applications using system features such as process fork and \ac{ipc}. 
Eleos~\cite{orenbach2017eleos} improve upon this work to implement exit-less system call operations and enclave controlled virtual memory management. 
This does however require some modifications to existing applications as Eleos uses software address translation via a C++ template pointer class for memory references.

\textcite{Tian:2017:SLO:3075564.3075572} implement a custom library \ac{os} kernel similar to Haven and Graphene.
SGXKernel uses techniques for asynchronous system calls adopted from the work in SCONE. 
The authors argue that enclave managed system calls replicate functionality present in commodity operating systems and advocate only presenting a minimal stub interface to enclave-hosted legacy applications.
SGXKernel reports that its secure runtime comprises only 7K lines of code excluding the 80K MUSL standard library.
However, this work does not describe how and if system-call integrity is preserved via encryption or replay prevention. 
Due to these shortcomings, we do not list them as a viable candidate for comparison in Table \ref{fig:tcb_loc}, as the threat model is potentially different.

\textcite{lind2017glamdring} implement automatic application partitioning using static data-flow analysis based on developer annotated source code. 
Sensitive components are automatically placed inside the enclave, and transitions are signed and encrypted. 
Analysis may move components into enclaves given evidence of a potential performance improvement, however does not implement any optimizations i.e. exit-less operations. 
Glamdring has a much lower TCB than comparative solutions.

Panoply~\cite{shinde2017panoply} focuses on reducing of \ac{tcb} rather than performance, and much like SCONE, Haven, and Graphene, enables execution of unmodified application binaries. 
Panoply uses a minimal shim for trusted mediation of system services implementing explicit enclave exit operations. 
Panoply does not implement IO encryption nor detail how replay attacks may be prevented. 

% client side javascript, webbrowser
\textcite{goltzsche2017trustjs} implement trusted execution of JavaScript through TrustJS. TrustJS may be integrated into a commodity browser, providing servers with an attested client-side component for storing/processing.


\begin{table}
\centering
\resizebox{\columnwidth}{!}{%

    \begin{tabular}{ l l l l l l l l }
    \hline
    \textbf{Secure runtime} & \textbf{Haven} & \textbf{Panoply} & \textbf{Graphene} & \textbf{SCONE} & \textbf{Google V8} & \textbf{Ductape} & \textbf{Diggi}\\ \hline
    \textbf{SLOC} & 5 000K & 20K & 53K & 97K & 17 000K & 185K  & 18K\\ 
	\end{tabular}%
	}

\caption{The \ac{sloc} of Diggi compared with similar work implementing secure runtimes in \ac{sgx}. 
\ac{sloc} is a commonly used metric to measure the \ac{tcb} of a software system, indicating the implementation complexity and the circumference of assumed trust. 
Measurements for SCONE, Graphene, Panoply, Ducktape and Diggi exclude the Intel provided \ac{sgx} \ac{sdk} code as well as stock implementations of standard libraries (glibc/musl). 
Depending on use, we expect this to add roughly 100K \ac{sloc}. As an exception, Haven depends on the Drawbridge library OS, and is likely not using gLibc.
}
\label{fig:tcb_loc}
\end{table}


\section{Distributed Systems and Coordination}
Remote attestation of processes simplify fault tolerance and distributed consensus, by assuming that coordinated entities are benign once attested.

%BFT systems
\textcite{behl2017hybrids} present a hybrid state-machine replication protocol which uses a trusted subsystem to assume a crash-stop fault model. 
This reduces the overhead of solving Byzantine Fault Tolerance (BFT) in scalable distributed systems.
%system, bFT
\textcite{troxy} enable the creation of decoupled BFT clients through trusted proxies situated on the server. 
Clients may transparently access a BFT system through the proxy, where traditional client code is executed on the server side. 
The benefit of this is easy upgradable consensus protocols, where clients are able to access replicas regardless of protocol version.
Bloxy~\cite{rusch2019bloxy} expands on this concept and applies them to blockchain protocols.

Teechain~\cite{lind2019teechain} implements efficient off-blockchain transactions using \acp{tee} as a secure \emph{treasury} for handling payment and settlement off-blockchain. 
Treasuries are replicated in a committee and vote on settlements onto the blockchain, asynchronously from the side-chain. 

%blockchain bitcoin
Lightweight client verification of blockchain payments may leak information about the client transactions.
\textcite{matetic2019bite} leverage \acp{tee} to protect full node verification on behalf of a lightweight client, while also addressing inherent side-channels exposed by the protocol.

%system, encanching com TOR
\textcite{kim2017enhancing} describe how the security and privacy of Tor anonymous routing network can be enhanced by using \acp{tee}. 
SGX-Tor reduces the possibility for an adversary to inspect or modify the software state of a Tor node. 
Additionally, the reduced trust model increases the potential to scale the Tor network without implicitly trusting nodes.

\textcite{Zhang:2016:TCA:2976749.2978326} introduce an authenticated data feed, Town Crier, to consume trusted data sources in execution of autonomous smart contracts on blockchains. TC acts as a mediation layer and backend store, between a smart contract provider blockchain, and existing HTTPS enabled websites.

\textcite{brenner2016securekeeper} implement SecureKeeper, a secure Zookeeper variant in \ac{sgx}, which preserve the confidentiality and integrity of potentially sensitive cloud configuration data.

In-network computing is proliferating in cloud infrastructures due to the influx of capable routing hardware. 
Software Defined Networking (SDN) and Network Function Virtualization (NFV) enable flexible backbone architectures, but inspecting real-time traffic to make smart routing decisions comes at the risk of privacy.
Diggi shares some architectural traits with other persistent distributed secure constructs such as network functions (NF).
\textcite{poddar2018safebricks} use trusted hardware(\ac{sgx}) to implement shielded NFs which only expose encrypted traffic to the cloud provider and preserves the integrity of NFs.
\textcite{trach2018shieldbox} implement a similar concept through \emph{Secure Middleboxes} and
\textcite{goltzsche2018endbox} propose outsourcing Middleboxes to network clients through trusted computing.

Google Asylo\footnote{https://github.com/google/asylo} and the Open Enclave Project\footnote{https://openenclave.io/} implement trusted open source frameworks for developing distributed \ac{tee} applications, decoupled from the hardware mechanisms implementing shielded execution.

\textcite{Brenner2019} implement a secure runtime for JavaScript FaaS hosting, based on the minimal footprint Ducktape JavaScript engine, comprising an order of magnitude more code than Diggi. 
They also evaluate the Google V8 engine, which comprises some 17 million lines of code.
Both use webpack to allow dependency bundling of legacy applications by downloading all dependent JavaScript code from the web. 
Code is read from untrusted storage into the secure context, leaving the system susceptible to malicious code execution. 
The benefit of measuring the initial enclave state using this approach is also greatly reduced as enclaves are generic interpreters/runtimes which allow arbitrary code to be executed without evidence in the initial attestation process. 
Moreover, all cohosted functions share a single secure runtime environment, and although interpreted, are subject to bugs which may cause information leakage.  
For compatibility reasons, the Google V8 engine further requires all enclave pages marked as writable, which reduces robustness in the presence of bugs.
\textcite{qiang2018se} implement a secure serverless runtime built on top of the OpenLambda project, however, functions in OpenLambda are similarly implemented in JavaScript and the runtime requires a significant amount of code executing within the secure \ac{tee} context.

%system, faas, sgxv2, apacheopenwhisk
\textcite{trach2019clemmys} mediate cold-start latencies in an \ac{sgx}-based \ac{faas} by using the second generation of \ac{sgx} capable hardware, introduced in desktop \acp{cpu} along with the 2019 Ice Lake generation of Intels Core architecture. 
By using the new dynamic memory allocation feature~\cite{mckeen2016intel}, enclaves are able to load and unload \ac{epc} pages on demand and in batches. 
Pages are loaded upon access, reducing the startup-time for each function significantly. 
Similarly to Diggi, Clemmy implements a novel messaging format for ensuring confidential and integrity preserving communication across multiple functions. 
Clemmy is implemented as a modification to the ApacheWhisk open source \ac{faas} framework, using the SCONE POSIX runtime described above, where \ac{sgx} based functions are implemented on top of a Python interpreter.
Additionally, Clemmy is able to verify execution order among multiple functions. 
Diggi is similarly able to verify function execution order, and unlike Clemmy, function internal execution ordering by message logging.

%system, similar to faas, and similar to Diggi
EActors~\cite{sartakov2018eactors} implements a related persistent distributed trusted computing paradigm.
Similar to Diggi, the EActors framework creates a simplified concurrent abstraction for composing applications consisting of multiple enclaves together.
EActors are similarly focused on asynchrony~(non-blocking) execution, a small memory footprint, and a low \ac{tcb}.

\section{Summary}
Our thesis is situated among a massive corpus of similar research into trusted computing in an untrusted cloud. 
Concurrent efforts have inspired and validated our work, and additionally pinpoints the unique concepts and contributions which separate Diggi from the rest.

Several works implement trusted efficient runtimes, and some even in the context of serverless systems.
However, to the best of the authors knowledge, Diggi is the only trusted serverless runtime supporting asynchronous flow-based persistent cloud functions.

Techniques for optimization such as exit-less communication and memory offloading via ephemeral storage are concepts which several related works use.
 Systems for logging state mutations for verifiability have been implemented in several domains, both for accountability and debugging.
 Trusted systems use hardware support to create tamperproof logging of system metadata for use in forensics.
  Diggi is, to the best of the authors knowledge, the only generic POSIX-enabled trusted runtime capable of accountable execution using log record and replay. 
 
 The next chapter will conclude this thesis answering our initial conjecture, and additionally, highlight opportunities for further research in the context of Diggi.
 



