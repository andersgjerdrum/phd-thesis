%!TEX root = thesis.tex

%:-------------------------- Preamble -----------------------

% Three languages are supported, which will be reflected in the logo on the front page. Pass the appropriate language
% specified as a class option to uit-thesis. Passing any other languages supported by babel will result in the default
% language on the frontpage. If no language is passed, the default is selected.
%  - USenglish (default)
%  - norsk
%  - samin
% The frontpage comes in two variants, Master's thesis and PhD. Master is default, use classoption 'phd' for the PhD version.
\documentclass[USenglish]{uit-thesis}

\usepackage[backend=bibtex,url=false,isbn=false,eprint=false,maxbibnames=50,maxcitenames=2,mincitenames=1]{biblatex}
\addbibresource{phd-thesis.bib}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{sectsty}
\usepackage{makecell}
\usepackage{tablefootnote}
\usepackage[export]{adjustbox}
\usepackage{outlines}

\renewcommand{\cellalign}{tl}

\paragraphfont{\normalsize}
\usepackage{listings}
\usepackage{lipsum}

\lstdefinelanguage{JavaScript}{
  morecomment=[l]{//},
  morecomment=[s]{/*}{*/},
  sensitive=true
}
\lstset { %
    language=C++,
    basicstyle=\footnotesize,% basic font setting
}

\lstnewenvironment{code}[1][]%
  {\noindent\minipage{\linewidth}\medskip 
   \lstset{#1}}
  {\endminipage}


\newtheorem*{sizeprinciple*}{The Size Principle}
\newtheorem*{Cohesion*}{The Cohesion Principle}
\newtheorem*{access*}{The Access Pattern Principle}
\newtheorem*{provision*}{The Pre-provisioning Principle}
\newtheorem*{isolation*}{The Isolation Principle}
\newtheorem*{affinity*}{The Affinity Principle}
\newtheorem*{pinning*}{The Pinning Principle}
\newtheorem*{async*}{The Asynchrony Principle}
\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}

\makeglossaries
\DeclareMathOperator{\sig}{sig}
\DeclareMathOperator{\send}{send}
\DeclareMathOperator{\compile}{compile}

% Add external glossaryentries
\loadglsentries{acronyms}
\newacronym{api}{API}{Application Programming Interface}\glsunset{api}
\newacronym{2api}{2API}{application programming interface}
\newacronym{d3}{D3}{Data-Driven Documents}
\newacronym{html5}{HTML5}{version 5 of the HyperText Markup Language standard}
\newglossaryentry{thesis}
{
  name=thesis,
  description={is a document submitted in support of candidature for an
    academic degree or professional qualification presenting the author's
    research and findings
    },
}
\newglossaryentry{lage}
{
  name={long ass glossary entry},
  description={is a long ass entry with a lot of text describing the properties of the glossary entry. Hopefully this spans some lines now.
  },
}


%\newcommand{\listdefinitionname}{My list of definitions}
%\newlistof{definition}{def}{\listdefinitionname}
%\newcommand{\definition}[1]{%
%  \refstepcounter{definition}%
%  \par\noindent\textbf{The Definition~\thedefinition. #1}%
%  \addcontentsline{def}{definition}
%    {\protect\numberline{\thechapter.\thedefinition}#1}\par%
%}


\setlength{\parindent}{1em}
\setlength{\parskip}{0em}

\begin{document}

%:-------------------------- Frontpage ------------------------

\title{Diggi}
\subtitle{A Distributed Serverless Runtime for Developing Trusted Cloud Services}			% Optional
\author{Anders Tungeland Gjerdrum}        
\thesisfaculty{Faculty of Science and Technology \\ Department of Computer Science}
\thesisprogramme{A dissertation for the degree of Philosophiae Doctor - March 2020}
%\ThesisFrontpageImage{example_image.jpg}	% Optional

\maketitle

%:-------------------------- Frontmatter -----------------------
\frontmatter

\begin{dedication}
Til Jan-Henry~(1956 - 2018)
\end{dedication}

%\begin{epigraph}
%\epigraphitem{The right man in the wrong place can make all the difference in the world.}{G Man}
%\epigraphitem{It's a-me, Mario!}{Super Mario}
% \end{epigraph}

\begin{abstract}
Cloud computing offers the convenience of outsourcing storage and processing power to a public shared environment.
Physical infrastructure is managed by the cloud provider, allowing hosted services to be deployed without any upfront investment.
Cloud infrastructure may additionally manage deployment, migration, scalability, and fault tolerance, transparently from the hosted service.
Serverless computing, and more specifically \ac{faas}, is a natural continuation of this trend, narrowing the computational scope down to individually deployable \emph{cloud functions}, which are scalable and billable on demand.

Contemporary cloud services require that sensitive data such as user identifiable information be protected from unauthorized access. However, a conventional cloud security threat models assumes that the underlying public cloud infrastructure can be trusted.
Physical attacks on server hardware conducted by an unfaithful employee may compromise the entire software stack.
Moreover, a compromised operating system or hypervisor may directly inspect information in less privileged execution contexts. 

Dedicated hardware such as \acp{tee} mitigate such attacks by enabling privileged application containers, protected from inspection by the untrusted underlying system.
Intel \ac{sgx} introduces one such hardware system implementing support for hosting secure segments of code and data (enclaves) on commodity x86-64 platforms.
Enclaves may be attested remotely, however the attestation evidence is limited to the enclave's initial state.
\ac{sgx} is considered feature rich compared to similar \acp{tee}, however, the threat model of \ac{sgx} leads to some architectural intrinsics which may impact the runtime performance.

This thesis present the design and implementation of Diggi; an efficient trusted cloud function runtime implemented in \ac{sgx}.
Diggi enables the development of secure applications, composed of multiple persistent and accountable cloud functions which may be jointly authenticated through co-attestation.
We demonstrate that the design of Diggi is practical, and additionally, that it reduces the overhead of \ac{sgx} compared with standard runtime execution techniques.
We further demonstrate the applicability of Diggi by implementing two pseudo-real application workloads demonstrating a database management system and a machine learning inference pipeline on top of the Diggi runtime.
\end{abstract}

\begin{acknowledgement}
I would like to express my sincere gratitude to those who made this dissertation possible.

First of all, i would like to thank UiT: The Arctic University of Norway, for hosting me and funding my research for the last 4 years, it has been an honor.

I would also like to extend my gratitude towards the technical staff and administration, both at the department and faculty level. 
Particularly i would like to thank Svein Tore, Jan, Maria, Kai-Even, Jon Ivar and Ken Arne for being able to answer just about any inquiry, be it acquisition of hardware, sick-leave or other administrative work; simplifying my life as a Phd-student.

My co-advisors, H{\aa}vard, Dag and Robbert have contributed with endless discussions on core computer science problems, and have thought me the fundamentals of distributed systems research, and for that i am forever grateful.
I would also like to thank the other Phd-students at our lab: Magnus, Enrico, Tor-Arne, and Aakash, for valuable insights and interesting discussions.
Additionally, i would like to thank Robert, Lars, Helge and Eleanor for their direct contributions to Diggi, both engineering work and in-depth discussion on trusted distributed systems. Moreover, Fred, for much needed input on scientific writing.

Research is hard on the spirit, and i would especially like to thank all my friends and family for supporting me and cheering me on throughout this process.

Lastly, a huge thank you to my life partner, Sigrun, without whom, none of this would have been possible. I love you.

%During the work on this thesis, life passed by as it so often does. 
%To describe the last 4-and-something years as uneventful would be a lying at best.
%My father passed away after several years of toil and sickness.
%My father-in-law nearly did, and of-course.. the entire world became inflicted as-well. 

\end{acknowledgement}

\tableofcontents
\printglossary[type=\acronymtype,title=Acronyms]

%:-------------------------- Mainmatter -----------------------
\mainmatter
%Dag forslag struktur(
%- 1 intro, motivation,tese,context,scope
%- 2 Related work, serverless, trusted computing
%- 3 Desing requirements, system model, measurements sgx.
%- 4 Implementasjon av Diggi.
%- 5 micro evaluering Diggi perf.
%- 6 Use case, Fintech, boronbase, burnerbox.
%- 7 Conclusion
\chapter{Introduction}
\input{chapter1/chapter1.tex}
\chapter{Serverless Computing}\label{chapter:serverless}
\input{chapter2/chapter2.tex}
\chapter{Trusted Execution Environments}\label{chapter:tee}
\input{chapter3/chapter3.tex}
\chapter{Design}\label{chapter:reqs}
\input{chapter4/chapter4.tex}
\chapter{Cloud Function API}
\input{chapter5/chapter5.tex}
\chapter{Runtime}
\input{chapter6/chapter6.tex}
\chapter{Evaluation}
\input{chapter7/chapter7.tex}
\chapter{Discussion}
\input{chapter8/chapter8.tex}
\chapter{Concluding Remarks}
\input{chapter9/chapter9.tex}

\printbibliography
\backmatter
\end{document}
