The Diggi serverless runtime is designed to shield applications from an untrusted public cloud. 
Diggi enables the development of complex distributed cloud services through the \emph{persistent accountable cloud function} abstraction.
The next two chapters will detail our prototype implementation of the Diggi system, introduced in the previous chapter, specifically conforming to the requirements stated in Section \ref{section:requirements}.

This chapter details the services endpoints available for developing secure, persistent and accountable cloud functions on top of the Diggi prototype runtime supporting Intel \ac{sgx}.


%TODO: mention zcstring zero copy operations.
%TODO: mention threadpool multiqueue internals.
%TODO: mention multiuse of interleaved asynchrony. fallback to yield for oldscool.
\begin{code}[language=C++, captionpos=b, label={lst:examplefunc}, caption={An example using the Diggi runtime \ac{api} to implement an echo function. 
\emph{func\_init} registers a callback task for handling incoming messages of the type \emph{ECHO\_MESSAGE}. 
The callback receives a \emph{msg\_async\_response\_t}, encapsulating the inbound message and a discretionary context pointer.}]
void func_echo_cb(msg_async_response_t *resp)
{
   auto api = (DiggiAPI*)resp->context;
   auto mm = api->GetMessageManager();
   auto new_msg = mm->allocateMessage(resp->msg, resp->msg->size);	
   new_msg->dest = resp->msg->src;
   new_msg->src = resp->msg->dest;
   memcpy(new_msg->data, resp->msg->data, resp->msg->size);
   mm->Send(new_msg);
}
void func_init(DiggiAPI *api)
{
   auto mm = api->GetMessageManager();
   mm->registerTypeCallback(function_echo_cb, ECHO_MESSAGE, api);
}
void func_start(DiggiAPI *api)
{   //noop
}
void func_stop(DiggiAPI *api)
{
   auto log = api->GetLogObject();   log->Log(LRELEASE, "Stopping the echo-function\n");
}
\end{code}

%\section{Diggi API}
The Diggi cloud function runtime exposes a set \ac{api}-endpoints for developers of cloud functions, comprising functionality for concurrency, configuration, debugging, storage, and communication. 
All runtime operations and \acp{api} are asynchronous~(non-blocking) to maximize the potential utilization per thread.
Functions are persistent, and sessions enable an invoker to request the same instance repeatedly, implemented as \emph{flows}. 
These behave similarity to reactive programmable systems~\cite{reactivesurvey}, where dynamic queries \emph{observe} incoming data streams and apply processing rules on-demand.
Reactive programming is beneficial for several application architectures including stream processing analytics, publish subscribe systems, and video on-demand services; all of which are difficult to implement in conventional stateless \ac{faas} systems.
An example function implementing an \emph{echo server} which retransmits incoming data back to the sender, is shown in Listing \ref{lst:examplefunc}.

\begin{code}[language=C++, captionpos=b, label={lst:Diggiapi}, caption={The Diggi cloud function \ac{api} class definition which acts as the aggregate \ac{api}, gathering a collection of interfaces comprising the joint functionality available to Diggi cloud functions. 
These include individual \acp{api} for storage, networking, messaging, concurrency, signaling and logging.}]
class DiggiAPI : public IDiggiAPI {
	IThreadPool * tpool;
	IStorageManager * stomanager;
	IMessageManager * msgmanager;
	INetworkManager * netmanager;
    ISignalHandler * shandler;
	ILog * logr;
	aid_t aid;
    sgx_enclave_id_t enclaveid;
	void * dl_handle;
	json_node configuration;
public:
	DiggiAPI(
        IThreadPool * pool, 
        IStorageManager * smngr,
        IMessageManager * mmngr,
        INetworkManager * nmngr, 
        ISignalHandler * sighandler,
        ILog * log, 
        aid_t id,
        void * handle);
	IThreadPool * GetThreadPool();
	IStorageManager * GetStorageManager();
	IMessageManager * GetMessageManager();
	INetworkManager * GetNetworkManager();
	sgx_enclave_id_t GetEnclaveId();
	ISignalHandler *  GetSignalHandler();
	void * GetDlHandle();
	ILog * GetLogObject();
	aid_t GetId();
	json_node & GetFuncConfig();
};
\end{code}
\newpage
\section{Lifecycle management}
Figure \ref{fig:funcapi} illustrates the lifecycle of a Diggi cloud function.
To handle lifecycle events, each cloud function must implement three event callback handlers: \emph{init}, \emph{start}, and \emph{stop}, as demonstrated in Listing \ref{lst:examplefunc} 
These handlers are invoked by the Diggi runtime to notify the cloud function of significant events altering execution state.
\begin{itemize}

\item \textbf{Init} -- Before application-defined task callbacks are allowed to execute, the runtime prepares communication primitives, stacks, storage interface and other internal data structures. 
The init event callback, defined by the cloud function, is ran once these initialization procedures are completed. 
This callback is executed serially by the runtime and may block to guarantee that all preconditions are met before the cloud function begins servicing incoming requests.
The init function is a utility for developers which should prepare function-specific internal state, initialize libraries, etc. 
The callback receives the Diggi \ac{api} object as input, exposing all runtime services available to the cloud function, detailed in Listing \ref{lst:Diggiapi}.
Once the callback is completed, the Diggi runtime expects that the cloud function is ready for execution, and will begin forwarding inbound messages.

\item \textbf{Start} -- Once ready, the start event callback is triggered by the runtime to notify the function that it is ready to process requests.
Both the init and start callback are triggered on the primary thread of the cloud function. However, unlike init this operation must be non-blocking.
If the cloud function has multiple threads, the start event will be scheduled on the thread with the lowermost identity. 

\item \textbf{Stop} -- Cloud functions persist for as long as the cloud function or runtime permits. 
When the execution of a cloud function ends, a stop event callback is triggered, either by the function itself or the runtime.
Prior to invocation, the runtime attempts to relinquish all dedicated threading resources. 
Any nested task callback operations remaining in the cloud function are aborted, and the physical thread resources returned to the runtime.
Cloud functions are permitted to gracefully terminate, reducing the potential for races in multithreaded cloud functions.
The callback is invoked by the runtime on a separate runtime thread to ensure any blocking operations internal to the cloud function do not prevent termination. 
This is a mandatory operation, implying that the runtime expects the function to cooperate in the cleanup process. 
Diggi does not guarantee that all shutdown events will be preceded by a notification via this callback. 
In the event of a system crash, this will be ignored.
\item \textbf{Idle} The system allows dormant functions which are not participating in active request processing to relinquish resources and enter an idle state. The runtime periodically polls for incoming requests and upon the receipt, returns resources to the cloud function. The cloud function does not require idle-aware programming; the runtime transparently suspends and awakes cloud functions.
\end{itemize}

%TODO: relate to existing systems
\section{Asynchronous Programming}\label{section:asyncimpl}
Asynchronous programming allows computational events to execute separately without a global synchronized clock. 
In an asynchronous system all possible ordering of computational events are valid and must be handled correctly.
Cloud functions in Diggi are implemented as \emph{continuation-style} asynchronous task callbacks, allowing efficient scheduling of runtime operations.
These chains of operations~(\emph{tasks}) are in Diggi referred to as \emph{flows}, which are the primary building block for application logic in persistent cloud function.
This abstraction renders the following properties for asynchronous programming of cloud functions.
The Diggi programming model is more restrictive than \emph{pure} asynchrony, where any ordering, causal or otherwise is permitted.
However, we present the Diggi API as asynchronous given the following assumptions. %TODO: how so.
For a given thread:
\begin{itemize}
	\item \emph{Non-Blocking}: All causally dependent operations which await a response must asynchronously schedule that continuation to allow other operations to execute in the interim.
	\item \emph{Starvation-free}: All operations which are compute-intensive must gracefully share resources by yielding control to other operations periodically.
	\item \emph{Intra-flow order}: All operations internal to a particular asynchronous flow will maintain causal ordering when executed asynchronously.
	\item \emph{Inter-flow interleaving}: Operations on separate flows may be interleaved and reordered.
\end{itemize}

Through flows, cloud functions are able to handle message delivery and business logic for a given function concurrently on a single thread without context-switches and enclave interrupts, which increases the co-hosting potential. 

\begin{code}[language=C++, captionpos=b, label={lst:asyncpattern}, caption={Example code illustrating the asynchronous polling pattern. These flows may alternatively be serialized into one flow by rescheduling \emph{func\_pool} in \emph{func\_finalize}. Both patterns permit interleaving separate operations.}]
void func_poll(DiggiAPI * api)
{
   if(item_present(pollable))
   {
      api->GetThreadPool()->Schedule(func_handle, api);
   }
   //Reschedule to periodically poll resource
   api->GetThreadPool()->Schedule(func_poll, api);
}
void func_handle(DiggiAPI * api)
{
   auto item = get(pollable);
   api->GetThreadPool()->Schedule(func_finalize, api);

}
void func_finalize(DiggiAPI * api)
{
   free(item)
   .
   .
}
\end{code}
\begin{figure}
\centering
\includegraphics[width=0.7\textwidth,page=19, trim={2cm 7cm 11.3cm 4cm}, clip]{figures/Diggiarchitecture.pdf}
\caption{Interleaving of three flows on a queue of tasks; blue is the polling flow, while red and green are separate flows processing items retrieved. Cloud functions may interleave communication and processing on a single thread.} 
\label{fig:implinterleaving}
\end{figure}

Listing \ref{lst:asyncpattern} presents this phenomenon for a single cloud function through a simplified representation of the continuation-style pattern. 
Figure \ref{fig:implinterleaving} illustrates an example execution plan demonstrating one (of many) correct orderings of interleaved asynchronous flows onto a task queue consumed by a single thread. 
One implements a periodic item polling, while the other handles available items. 
In the illustration, two separate items become available, resulting in three flows~(red, blue, green).

The Diggi runtime is itself internally implemented through tasks and flows, and all Diggi \acp{api} exposes a consistent pattern for passing state between tasks, illustrated in Listing \ref{lst:codingpattern}.

\begin{code}[language=C++, captionpos=b, label={lst:codingpattern}, caption={
An example pseudocode pattern illustrating how Diggi preserves asynchronous state and delivers it to the next task, once ready.}]
task(arg)
{
   internal_state = input.operation();
   schedule(task_next, internal_state);
}
task_next(void *internal_state)
{
   internal_state.next_operation();
   ...
}
\end{code}

This approach solves a common problem in asynchronous systems; preserving state between asynchronous operations adds complexity to the system. 
By allowing Diggi to manage state internally, we minimize the boilerplate code necessary for asynchronous communication, and additionally incur a potential reduction in \ac{tcb}.
%TODO: informal comparison asynchronous boilerplate code.

\section{Programming Language}
Cloud functions are commonly developed in high-level programming languages, such as JavaScript or Python. 
High-level languages are beneficial for rapid development, as most of the low-level details such as explicit memory management and library loading are handled automatically by the runtime.
Because of this simplicity, high-level languages also have a bigger developer base than their low-level counterparts. 
High-level languages offer features such as automated memory management by garbage collection and byte code interpretation for portability across different hardware platforms. 
Some high-level languages provide \ac{jit} compilation which ship code in an intermediate language representation, which is then compiled on-demand to native platform dependent code. 
This ensures higher performance without sacrificing portability.

As a general rule, simplification is a tradeoff which inherently sacrifices performance.
Code requiring precise performance tuning, such as cache alignment, memory management, lock-free concurrent programming, and instruction set extensions such as x86 AVX, SSE or TSX will benefit from explicit control over the underlying architecture.

Additionally, high-level languages contribute negatively to the \ac{tcb} of a trusted enclave, requiring more runtime code and system libraries than its compiled counterpart.
\ac{jit}-ed languages require dynamic linking and memory mapping features not available in the first revision of \ac{sgx}.
Supporting interpreted languages in \ac{sgx} is straightforward, however, loading generic virtual machines into an enclave violates the ability to perform program attestation. 
Arbitrary programs may execute inside the enclave, and careful engineering is required to ensure the identity of interpreted code is included in the attestation evidence. 
Arbitrary code execution additionally increases the attack-surface of the enclave.
Runtime and library support for high-level languages incur a larger memory footprint, contrary to the Size Principle stated in Section \ref{section:principles}.

Enclave-mode permits direct access to host process memory, similar to other \acp{tee}.  
Interpreted languages may provide memory safety, reducing the potential for memory reference bugs which accidentally writes unshielded data to a non-trusted memory location.

Rust is a high-performant, lean, compiled language which mediates memory safety problems inherent in C/C++ development during compile-time, disallowing null pointer references and enforcing explicit pointer-object attribution and ownership~\cite{matsakis2014rust}.
Diggi currently supports cloud functions through C and C++, with on-going work to support the Rust programming language in the future.

The \ac{sgx} \ac{sdk} supports enclave development through C/C++ out of the box, including library support for most of \ac{stl} and gLibc. 
However, this excludes all functionality which require system services, such as network, filesystem, time, crypto and peripheral devices. 

\section{Legacy}\label{section:legacyimpl}
Ideally, Diggi would be able to support a broad software library, enabling all applications to be developed as native asynchronous and flow-based cloud functions, reducing the need for external dependencies. 
This is, however, impractical due to the the increased \ac{tcb} and memory consumption required~\cite{baumann2015shielding}. 
Additionally, existing software libraries are often tied directly to the host system call api exposed by the \ac{os}.
To support existing~(legacy) software libraries in a shielded serverless systems, Diggi exposes runtime constructs to allow cloud functions to implement select storage, concurrency, and networking system calls; securely and asynchronously.

Traditional \emph{blocking} system calls in procedural programming expect execution to transfer control to the system for processing halting execution of the caller.
Diggi cloud functions are dependent on \emph{wait-free} task execution to maximize thread utilization. 
Existing software depending on blocking procedures to synchronize application progress cannot easily be refactored.
To avoid costly context-switches, operations must be fulfilled through exit-less asynchronous message-passing. 

Diggi saves the execution context of calling code and allow a dedicated \emph{translator} to prepare an asynchronous message representing the operation which is then delivered to the untrusted system. 
The translator must, depending on operation, protect the integrity and confidentiality of the message.
The untrusted system then processes the message via a \emph{server}, interacting directly with the host operating system. 
The response is then delivered back to the translator and the task scheduler then restores the execution context of calling code before returning to the caller with the translated results.
During this procedure the caller yields control, permitting concurrent operations to be scheduled on the task queue, as depicted in Figure \ref{fig:continuation}.

Each translated system call will increase the exposure for Iago-based attacks~\cite{checkoway2013iago}.
Some may be emulated in enclave-mode, avoiding the need for interacting with the untrusted system.
Networking operations do not change the security model based on an untrusted system, as \ac{wan} communication is expected to be protected by \ac{tls} or application layer specific measures such as \ac{pgp} and \ac{shttp}.
Other system calls such as \emph{time} and \emph{rand} are difficult to emulate in enclave-mode, and impossible to shield by translation. 
These may be fetched or pre-provisoned~(cached) from a remote trusted resource. 
\ac{sgx} supports trusted time and randomness through dedicated platform services~(Intel Management Engine). 
However, these are implemented in firmware and only retrievable through pre-provisioned platform enclaves which incur a significant invocation overhead.

To support legacy software which require system interaction, a cloud function implements for each external service, a translators and server pair, as illustrated in Listing \ref{lst:translatorserver}.
The server implements the untrusted host system interaction, while the translator mediates system call access and protects the confidentiality and integrity of translated system call operation.

%TODO: identify techniques for emulation/translation in legacy chapter.
Applications must themselves ensure that the translator shields the system call arguments correctly, and validates the return parameters, to ensure no tampering.
For data system calls, such as network and storage, this implies ensuring confidentiality and integrity protection.
Translator-server pairs are satisfied as messages and may be distributed across multiple machines, transparent to the calling software. 
For example, secure timestamps may be pushed from a trusted machine external to the public cloud.
Depending on the synchronization requirements of the calling software, timestamps may be cached if only monotonicity is required.
Cloud function-specific translators allow each to tune the semantic behaviour and security of system calls individually.
The caveat, however, is an increase in complexity for developing cloud functions, where each must implement support for bespoke system call operations. 
However, exposing a simplified translator-server \ac{api} to cloud function developers reduces this complexity. 
 
\begin{code}[language=C++, captionpos=b, label={lst:translatorserver}, caption={A server/translator pair implementing the open system call. For brevity, this example does not include code for encryption or integrity protection.}]
\\ Server
void open_server_task(void *msg, int status)
{
    auto ctx = (msg_async_response_t *)msg;
    auto ptr = ctx->msg->data;
    mode_t md = Pack::unpack<mode_t>(&ptr);
    int oflags = Pack::unpack<int>(&ptr);
    int encrypted = Pack::unpack<int>(&ptr);
    /* assumes null terminated character */
    const char *path = (const char *)ptr;

    /* Direct system call */
    int fd =   __real_open(path, oflags, md);
    
    auto msg_n = api->GetMessageManager()->allocateMessage(
                                                ctx->msg, 
                                                sizeof(int));
    msg_n->src = ctx->msg->dest;
    msg_n->dest = ctx->msg->src;
    auto ptrt = msg_n->data;
    Pack::pack<int>(&ptrt, fd);
    api->GetMessageManager()->Send(msg_n, nullptr, nullptr);
}

\\ Translator
int open_translator_task(const char *path, int oflags, mode_t mode)
{
    char *path_n = normalizePath((char *)path);
    auto path_length = strlen(path_n);
    size_t request_size = sizeof(int) 
                        + sizeof(mode_t) 
                        + sizeof(int) 
                        + path_length 
                        + 1;
    auto msg = api->GetMessageManager()->allocateMessage(
                                            "Server-Destination", 
                                            request_size, 
                                            CALLBACK, 
                                            CLEARTEXT);
                                            msg->type = FILEIO_OPEN;
    /* Marshall */
    auto ptr = msg->data;
    Pack::pack<mode_t>(&ptr, mode);
    Pack::pack<int>(&ptr, oflags);
    Pack::pack<int>(&ptr, (int)encrypted);
    memcpy(ptr, path_n, path_length + 1);
    mngr->Send(msg, set_response, ctx);
    msg_t *single_p_response;
    msg_t **double_p_response = &single_p_response;
    double_p_response = nullptr;
    /* Wait and yield to task queue */
    auto return_msg = wait_for_response(double_p_response);
    \*Unmarshal results*\
    int fd = Pack::unpack<int>(&return_msg->data);
	return fd;
}
\end{code}


\section{Deployment}

\begin{figure}
\begin{center}

\begin{tabular}{c}
\begin{lstlisting}
"Diggi-node-1": {
 "network":"192.168.1.137:6000",
 "functions": {},
 "enclave": {
  "functions": {
   "echo-function@1": {
    "skip-attestation": "1",
    "threads": "1"}}}},
"Diggi-node-2": {
 "network": "192.168.1.138:6000",
 "functions": {
  "load-function@1": {
   "skip-attestation": "1",
   "master": "1",
   "package-size": "87040",
   "connected-to": "echo-function@1",
   "threads": "1",
   "duration": "10" }}}
\end{lstlisting}
\end{tabular}
\end{center}
\caption{An example Diggi application configuration, consisting of two functions; an echo-function and a load-function.}
\label{fig:config}
\end{figure}
Serverless applications may be composed of multiple persistent cloud functions, specified through an application configuration. 
The application configuration specifies co-hosting properties, placement and resource usage.  
Each cloud function has a sub-configuration for specifying custom input parameters.
Rich configuration options may simplify development, however, generalize the cloud functions.
General code will reduce the identity properties of enclave measurement.
To avoid generalizability, the sub-configuration is itself compiled and linked into each function binary, becoming part of the enclave identity by measurement.
An example application configuration is listed in ~\ref{fig:config}, which implements a server~(\emph{echo-function}) based on Listing \ref{lst:examplefunc} interacting with a client~(\emph{load-function}).


During compilation, the configuration is used as a recipe for creating the units of deployable binaries, later distributed to the target hosts.
The build environment must be hosted on a fully trusted software and hardware stack, and able to communicate securely with the trusted principal authority for program authentication.

For each individual cloud function, the build environment creates a deployment key pair, \(\{P_{d}, S_{d}\}\). 
The binary \(B_{i}\) and the sub-function configuration \(C_i\) is measured and signed by the private key \(S_{d}\), forming the SIGSTRUCT certificate:
\[
Cert_i = Sign(S_{di}, Measurement_{i}(B_i,C_i))
\]
Each \(F_i\) includes the binary \(B_i\), per function deployment configuration \(C_i\) and the certificate \(Cert_i\): 
\[
F_{i} = \{Cert_i,\{B_i,C_i,\}\}
\]

Before cloud functions are deployed to physical hosts, a daemon host process and untrusted runtime must be initialized.
The Diggi trusted root distributes binaries according to the placement specifications in the application configuration.
For the deployable set of functions \(D_N\), the individual function bundles and the per-node config \(C_N\) are distributed to a given node \(N\):
\[
D_N = \{F_1, F_2, ... F_i, C_N\}
\]

The daemon process deploys applications according to the configuration by creating an enclave for each cloud function binary as described in section \ref{section:enclavelifecycle}.
The \ac{le} compares the actual measurement of the cloud function binary against the expected measurement and signature:
\[
	\forall{i}, Verify(F_i,P_{di}) \land (M_{hw} = M_i)
\]
Once completed, the cloud function may be certain that it is an authentic cloud function signed by the trusted root, identical to the binary submitted by the developer.

%TODO: mention public cloud more explicitly?(dag asks)
\section{Summary}
This chapter details the implementation of the Diggi \ac{api} for developing secure serverless applications in an untrusted cloud, through a collection of persistent, shielded and accountable cloud functions.
Diggi is implemented from the ground up to support a performant trusted runtime in \ac{sgx}, through asynchronous programming.
The Diggi \ac{api} facilitates cloud function development through an asynchronous task and flow based abstraction built on top of efficient message-passing interfaces. 
Cloud functions may implement rich features through existing libraries by translating system dependancies into translator/server pairs, where procedural system call operations are translated into asynchronous messages.
The next chapter will detail the Diggi daemon process, the trusted and untrusted asynchronous runtime, responsible for efficiently hosting collections of shielded cloud functions.
