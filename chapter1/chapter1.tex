Connectivity is considered an essential part of modern life.
The Internet offers new ways to manage personal memory, interaction, and consumption through online services such as cloud storage, social media, instant messaging, and online shopping. %TODO: problem rephrasing consumption
Connected devices perceive and record large quantities of personal information enabling online services to infer and provide \emph{intelligent} capabilities to end-users.
Monetization occurs through the promise of access to rich, convenient and delightful services in exchange for targeted advertisement.

Physical infrastructure for hosting online services is often managed by a public cloud; an \ac{iaas} provider. 
Delegating the management of infrastructure reduces operational expenses, increases service reliability, and provide more predictable cost estimates~\cite{fox2009above}.
The risk of investment is reduced, as cloud infrastructure is rented rather than owned.

Hosting privacy sensitive data and computations in a public cloud requires that services trust the underlying infrastructure. 
Infrastructure may be compromised by exploits or tampering, rendering the upper layers of the software and hardware stack visible to an attacker.
An unfaithful employee of the cloud provider could potentially mount a privileged physical attack against a hosted service. 
Additionally, software may contain misconfigurations or bugs, which cause involuntary information leakage.
System software is particularly susceptible to bugs and misconfigurations due to the complex nature of low-level engineering.
Supply-chains may also be compromised by tampered infrastructure, where logging devices may be placed on hardware system buses~\cite{wiredsupplychain}.
Given these concerns, privacy-compliant online services should implement techniques to shield application software and data from the untrusted infrastructure.

For modern cloud services, composability and separation of concerns simplify scaling and fault tolerance.
\emph{Microservices} provide a composable abstraction for developing complex and highly scalable cloud services~\cite{newman2015building}.
Unlike traditional monolithic services, microservices are developed as multiple single-purpose distributed units of application logic.
These loosely coupled components present a composite service through well defined networking protocols such as \ac{http}, Apache Thrift~\cite{apachethrift} or Google \ac{rpc}~\cite{googlerpc}. %TODO: vurdere om skal i cites istedenfor
Microservices are commonly deployed at scale using container technology, implementing an isolated and virtually dedicated \ac{os} despite a shared infrastructure.
Deployment, fault tolerance and scaling is simplified using technologies such as Docker Swarm~\cite{dockerswarm} or Kubernetes~\cite{burns2016borg}. 

Serverless computing~\cite{theriseofserverless} iterates on these technological achievements to offer automatically managed cloud infrastructure.
\emph{Cloud Functions}, or \ac{faas}, are the primary manifestation of this paradigm, reducing the unit of scalable computation to individual event-driven functions, deployed on demand.
Events may trigger function invocation through client requests, message queues, database changes, or timer-based operations. 
Complex online services may be implemented as collections of cloud functions interacting in synchrony.

Most major cloud providers support cloud functions, including \ac{aws} Lambda Functions~\cite{AWSLambda}, Microsoft Azure Functions~\cite{azurefunctions}, and Google Cloud Functions~\cite{googlecloudfunctions}.
This single-purpose service-oriented abstraction automates many properties of cloud software, including fault tolerance, scalability, availability, and placement.
Application code is decoupled from explicit knowledge of the underlying computing resources, allowing services to automatically scale on demand, hence the moniker \emph{serverless computing}~\cite{hendrickson2016serverless}.
Compared to traditional cloud hosting in which servers are rented per \emph{time unit}, serverless functions are rented per \emph{invocation}~\cite{jonas2019cloud}.

This thesis investigates techniques for shielding contemporary cloud services from an untrusted infrastructure, including the replacement of current shielding techniques with more efficient ones. 
We include a preliminary analysis of available technological foundations for securing software in an untrusted environment, and additionally a selective empirical study of eligible technology.

\section{Trusted Execution Environments} 
Modern commodity processors implement hardware support for shielding applications from an underlying system~\cite{santos2014using}~\cite{mckeen2013innovative}. 
\acp{tee} enable hosted code to execute in secure and trusted \emph{compartments} without requiring explicit trust in the underlying platform.  
Distinct CPU modes separate secure and non-secure execution; providing the ability for secure modules to remain encrypted in memory during execution.
The system bus will prohibit all access requests to \emph{secure memory} from the non-secure execution mode. 
Code executing inside a secure module is able to prove the correctness of both software and hardware to a third party.
This is achieved through \emph{software attestation}, a process in which the trusted hardware produces a \emph{quote}, containing a signed hash of the secure modules' code and data. 
The key used to sign the hash, or \emph{measurement}, is derived from material unique to the hardware platform.
By serving this quote upon request, the secure module is able to prove the following two properties:
the identity of the initial state~(code and data), and the authenticity of the hardware platform, firmware and trusted platform services.

%\subsection{Intel SGX}  
The most mature and available \ac{tee} is Intel~\ac{sgx}.
Although proprietary, \ac{sgx} has since release received significant scrutiny from the research community~\cite{arnautov2016scone,baumann2015shielding,brenner2016securekeeper,van2018foreshadow,lind2017glamdring, Zhang:2016:TCA:2976749.2978326,schwarz2017malware,shih2017t,gotzfried2017cache,orenbach2017eleos,weisse2017regaining,zheng2017opaque,tsai2017graphene}.
Most Intel \acp{cpu} developed after 2015 support \ac{sgx}, both in server-grade hardware and client desktops/laptops\footnote{github.com/ayeks/SGX-hardware}.

\ac{sgx} allow multiple mutually distrusting \emph{enclaves} to run concurrently on a single physical host. 
This property is unique to \ac{sgx}, whereas other \acp{tee} only provide a single \emph{secure world} per physical host~\cite{santos2014using}.
All parts of the secure platform are implemented in signed firmware or hardware~\cite{costan2016intel} and no operation conducted by the trusted hardware is visible to the untrusted host operating system.
We observe that the compartmentalization of software, emblematic of serverless computing, lends itself elegantly to the \emph{enclave} programming abstraction. 

%\subsection{Hardware Limitations} 
\ac{sgx} enclaves are compiled and deployed as regular shared library objects, however, limited by memory consumption. 
During boot-up, \ac{sgx}-cabable firmware sets aside a range of dedicated physical memory exclusively for \ac{sgx}. This is currently restricted to 128MB.
Over-subscribing memory will cause physical pages to be multiplexed among multiple enclaves; similar to conventional virtual memory. 
There is, however, an additional performance penalty for multiplexing secure memory~\cite{gjerdrum2017performance}.
The initial version of \ac{sgx} only permits enclaves to statically allocate memory at creation time, an issue addressed in a later revision~\cite{mckeen2016intel}

Enclaves execute in a higher privilege-level than the surrounding system, yet relies on untrusted system software to handle interrupt processing and resource management.
Interrupts generated by system software and hardware must explicitly exit this privileged mode before being serviced.
This indirection adds a significant performance penalty.

Enclaves may be authenticated by remote attestation, however submitted evidence is solely based on the initial measurement identifying its predicate state. 
As execution progresses, state is mutated, diverging from the initial identity. 
For long running enclaves, authenticating the initial state is a weaker identity for the mutated state.

The threat model for enclaves further provides no guarantee for verifiability of execution.
An enclave may be interrupted or subverted by the host, and determining whether an event has verifiably occurred requires additional security measures.

%\subsection{Threat Model}
Code executing inside an \ac{sgx} enclave considers the environment outside to be untrustworthy, including the operator of the hardware.
The untrusted software and hardware may actively attempt to subvert execution in order to gain access to data. 
\ac{sgx} protects the integrity and confidentiality of code and data inside an enclave.
The authenticity of the hardware platform, firmware and code executing inside an enclave may be verified remotely through attestation. 
Secrets may, following a successful attestation process, be securely provisioned to the enclave through a confidential and integrity preserving communication channel.
All interaction with the outside world is visible to an attacker and may be stored, modified and replayed back to the enclave at any point. 
SGX does not protect against exploitable side-channel attacks such as cache analysis~\cite{gotzfried2017cache}, however modifications to software and/or hardware have proven to harden systems against such attacks~\cite{oleksenko2018varys}

\ac{sgx}, and \acp{tee} in general, do not protect from denial-of-service attacks which prevent application code from progressing.
The underlying system may actively withhold resources, such as network packets, memory pages, I/O-resources or thread time-slices.

\section{Thesis Statement} 
Serverless computing is a contemporary cloud service abstraction which simplify deployment, scale, management and billing of distributed applications in a public cloud~\cite{jonas2019cloud}.

Physical attacks, bugs and misconfigurations may compromise cloud infrastructure, rendering less privileged services exposed.
A protected runtime should shield privacy-sensitive data from an untrusted cloud and protect the authenticity and verifiability of execution.
\acp{tee} enable cloud hosted, and authentic software, shielded from an untrusted underlying infrastructure. 

Developing an efficient serverless application runtime capable of \acp{tee} requires significant work in analysis, design and engineering.

The main hypothesis of this thesis is therefore:
\newline
\begin{center}
\textit{\textbf{\acp{tee} can be leveraged to build a secure and efficient serverless application runtime for trusted computing in a public cloud.}}
\end{center}
\mbox{}
\newline
This thesis will have particular focus on the design and implementation of \emph{Diggi}, a distributed runtime for secure native cloud functions. 

To evaluate the applicability of \ac{tee} in context of serverless computing, we conduct a precursory survey of serverless computing systems, detailing the opportunities and challenges in implementing a secure serverless runtime. 
The set of capabilities supported by \ac{sgx} suggests it to be the most applicable \ac{tee} for cloud computing. 
To validate this claim, we compare it to alternative candidate \acp{tee} and capable trusted hardware systems. 

A comprehensive baseline performance analysis of the programming primitives comprising the \ac{sgx} platform will allow us to deduce a set of general advisory principles for implementing efficient and secure systems using Intel \ac{sgx}. 
Insights gathered from this analysis will then be integrated, along with the threat model, into a complete system outlining the design requirements of the Diggi runtime.

To demonstrate that the Diggi runtime is practical, we will implement a prototype satisfying these requirements.
Our evaluation will confirm this practicality, by micro and macro benchmarks. 
This includes a layered analysis of the compounding effects of different security measures implemented in Diggi. 
Additionally, to demonstrate applicability of our prototype system, we implement a rudimentary application simulating a privacy-sensitive workload.
%We expect our prototype system evaluation to sufficiently demonstrate the affirmation of the thesis conjecture.
\section{Scope and Limitations}
We design and implement a prototype serverless runtime for trusted execution in a public cloud. 
Completeness is not a first-order concern and throughout, this thesis assumes a set of properties at the boundary of our limited prototype design:
\begin{itemize}
	\item Cloud providers are able to host Diggi on top of bare-metal physical hosts or equivalent virtual machines which make \ac{sgx} capabilities available to the guest \ac{os}.
	\item Legislative requirements may altogether prohibit hosting certain privacy-sensitive data in a public cloud. However, we conjecture that the techniques described here may partially alleviate articles in regulatory frameworks such as the \ac{gdpr}~\cite{gdpr} or the \ac{hipaa}~\cite{hipaa}. 
	\item Although automatic management and scaling of cloud functions is a core feature of serverless computing, we refrain from discussing it in significant detail in this dissertation. We consider this an engineering problem, orthogonal to the focus of this thesis. We primarily focus on the development of a secure \emph{runtime} for hosting cloud functions, and where applicable, we demonstrate scaling potential to support this claim.
	\item  Diggi assumes a crash-stop~\cite{crashstop} model for all distributed processes, and assume benign execution of authenticated functions in \acp{tee}. Runtime behaviour violating these preconditions, will cause the system to intentionally crash.
	\item Although Diggi cloud functions demonstrate persistence, we consider state in functions to be ephemeral and state is not replicated. However, persistence is not a fundamental limitation to serverless computing; future system may include the ability to maintain distributed persisted state, with application-tailored consistency models.
	\item Serverless platforms isolate the host operating system and cohosted tenants using virtual machines. Diggi supports cross-tenant isolation but does not implement host protection. We consider this problem complementary and previous works demonstrate that a solution is practical~\cite{arnautov2016scone}~\cite{hunt2018ryoan}.
\end{itemize}
\section{Methodology}
Modern science may be defined as the endeavor of repeated systematic study of phenomenon, both ethereal and physical. 
It encompasses the organization of knowledge into verifiable or reproducible claims, of which further study is built upon.
Following controversy, scientific progress is achieved when reaching consensus on claimed truths. 
Natural, or formal sciences, follow the hypo-deductive method~\cite{godfrey2009theory}. Observations of processes or phenomena lead to the formation of a generalization in form of a hypothesis, tested by logical deduction or experiments.
This process is iterated until the test results matches the expected outcome, i.e there are no errors in the experiment.

The final report of the ACM Task Force on the Core of Computer Science~\cite{denning1989computingasadiscipline} presents a new taxonomy for classifying computing as a science.
Computing research is rooted in three paradigms, \emph{theory}, \emph{abstraction} and \emph{design}.
\emph{Theory} is the foundation for logical reasoning and mathematical sciences. 
\emph{Abstraction} is the applied method of natural sciences, where the formation of hypothesis and models are validated through experiments.
\emph{Design} is defined by the iterative process of solving problems based on specification and implementation.
\newline

\textbf{Theory} is rooted in mathematics and consists of the following procedure:
\begin{enumerate}
	\item Describe or characterize the phenomenon. 
	\item Pose a hypothesis based on the characterization.
	\item Prove the hypothesis by way of logical deduction to determine the truthfulness of the characteristics.
	\item Analyze the results, testing the proof to the object or phenomenon in observation. 
\end{enumerate}
These steps are iterated for as long as errors or inconsistencies are present.
\newline

\textbf{Abstraction} is founded in the experimental scientific method with 4 distinct stages:
\begin{enumerate}
\item Form a hypothesis based on the reasoned relationship of phenomenons or expected logical outcomes. 
\item Construct a model representing this hypothesis, and predict observed behaviour.
\item Design experimental parameters to increase the certainty of the phenomenons. 
\item Collect and analyze empirical evidence from the resulting experiments in congruence with the initial hypothesis. 
\end{enumerate}
The stages may be iterated upon until the hypothesis successfully predicts observed behaviour.
\newline

\textbf{Design} is founded in the practical engineering discipline, and involve the following steps:
\begin{enumerate}
	\item Based on a series of observation, state a set of requirements for which a system must fulfill.
	\item Design and implement a prototype system according to these specifications.
	\item Construct a set of tests to evaluate the system in conformance with the initial requirements.
\end{enumerate}
These steps may be repeated until the requirements are fulfilled.
\newline

The paradigms are archetypes of the scientific method, and computer science borrows from all three. 
In algorithm research, the construction of algorithms to model the characteristics of a phenomenon and the formal proof thereof, are funded in theory.

In machine learning, or computational intelligence, hypotheses are formed based on a expected correlation of separate phenomenon. A learned model is then constructed to predict this correlation, and experiments validate the conformance through classification. 

Software engineering describes an initial problem statement through requirements and specifications of a system, which is implemented and its validity tested in conformance with the requirements.
Although all computer science is permeated by theory, computer science is not only a science of the artificial~\cite{denning07}. 
Information processes and computing preexist the earliest descriptions of physical computing devices. 

This thesis is funded in the subfield of computing referred to as \emph{systems research}, mixing abstraction, theory and design.
We use abstraction to describe the proprietary Intel \ac{sgx} platform, and test our hypothesis by experimental measurements. 
Additionally theory, to reason about the security properties of our hypothetical system, proposing algorithmic solutions to preserve confidentiality, integrity and authenticity. 
Based on the proposed hypotheses, we design a system model through a set of requirements. 
We then implement a prototype system, \emph{Diggi}, and evaluate its conformance with stated requirements through experimentation.

\section{Research Context}
The research presented in this dissertation was conducted in the context of the \emph{Corpore Sano} research group, exploring the intersection between computer science and life sciences. 
Notable research targets elite sports performance development and injury prevention, preventive healthcare, large scale population screening, and epidemiological health studies. 
By applying systems research to these fields, Corpore Sano aims to disrupt state-of-the-art monitoring and analysis within the field.
%TODO: get some paper references from dag. muitu, baggadus, davi. football gastro.
In support of these goals, systems research conducted by the group covers all abstraction of the software stack. 
This includes data collection and processing, big data and machine learning applications, fault tolerance, and software mechanism for privacy and policy enforcement.

The Corpore Sano center is a natural continuation of research conducted as part of the iAD~(information Access Disrupted) research group, funded by the Norwegian Research council as a center for research driven innovation (SFI~2007-2015).
To place this dissertation in context, we summarize relevant previous research contributions by the group.

With the advent of commodity support for hardware based virtual machines, the public cloud enable consolidation of computing resources to provide \ac{iaas}.
The Vortex omni-kernel~\cite{kvalnes2014omni} implements a cloud centric operating system from the ground up to support resource isolation between tenants. 
Vortex introduced a novel approach to resource scheduling and attribution using message aggregates which delegate messaging resources based on policy.

The Internet revolution lead to vast improvements in indexed knowledge, and introduce information retrieval tools to a global audience.
To ensure the relevance of retrieved content, sophisticated processing tools and computation pipelines are necessary.
Cogset~\cite{valvaag2013cogset} implements a distributed big-data processing engine based on the map-reduce pattern~\cite{dean2008mapreduce} exploring a novel approach to scheduling compute tasks based on data-locality, leading to an increase in performance relative to competing systems~\cite{white2012hadoop}.

In publish subscribe systems, active queries are able to process streams of information, ensuring responsive and practical inference on data.
Streaming queries are generally stateful constructs which are difficult to distribute across multiple physical hosts.
\textcite{brenna2009distributed} extend the Cayuga~\cite{brenna2007cayuga} stream processing engine to enable scalable distribution of queries represented through non-deterministic finite state automata.

Peer-to-peer systems promises a highly scalable way of organizing computing resources into structured or unstructured groups. 
Systems which implement decentralized computing are more fault tolerant and elastic than their centralized counterparts.
The blockchain~\cite{swan2015blockchain} is the most prominent example of a decentralized architecture which share a common ancestry to these systems.
Fireflies~\cite{johansen2015fireflies} implemented a practical scalable overlay network which supports Byzantine Fault-Tolerant~(BFT) membership agreement. 
By organizing members into a probabilistically verifiable pseudo-random network, attackers are unable to modify membership views of correct participants.

Cloud providers promise large scale computation for enterprises at low cost and low risk of investment. 
However, enterprises may have restrictions in migrating workloads onto the cloud. 
Policies such as lack of vendor lock-in, hybrid clouds, data locality, and retention, require precisely tagged and described data.
Balava~\cite{nordal2011balava} investigates data migration between heterogeneous cloud providers using \emph{metacode}, including combinations of private and public (hybrid) clouds. 
Balava reduces vendor lock-in, and enables timely migration of on-premise software stacks onto the cloud.

\acp{cdn} provide a simple way of caching read-only data close to consumers, such as video on-demand services.
This read-only infrastructure is not well suited to handle write workloads and dynamically adaptable content.
Jovaku~\cite{pettersen2014jovaku} investigates reusing existing cloud infrastructure to create a middle-tier caching layer using the \ac{dns}. 

Ensuring authentic access to sensitive data is adamant to develop secure distribution. 
\textcite{renesse2013secure} implement a mobile smart access control abstraction through meta-code embedded in x509 certificates.
LoNet~\cite{johansen2015enforcing} implements policy enforcement through automated transparent information-flow de-identification enforced by an inter-positioned reference monitor between data and requestor.

Proof of concept implementations of these systems address many initial questions emerging from the cloud revolution.
As cloud technology has matured, more diverse frameworks which simplify development of cloud services push more of the management responsibility onto the infrastructure provider. 
The container abstraction lets the developer focus on software development, rather than operating system management.
Serverless computing reduces the scope of computing further, automating management and scaling completely.
As more aspects of cloud software is managed by cloud providers, consumers still require strict privacy and security guarantees for software executing in the public cloud.
This thesis focuses mainly on the mechanism necessary for secure distributed analytics and data processing in the cloud. 

\section{Impact}
Outside of publications, the work presented in this dissertation have afforded several distinct and noteworthy contributions and collaborations which are listed here.

As part of special curriculum requirements for master students at \emph{UiT: The Arctic University of Norway}, two student project assignments were written based on the foundations of Diggi. 
These explored the development of an encrypted file system in \ac{sgx} supporting durable storage of secrets, and an investigative survey on software replacements for \acp{tee} using homomorphic encryption schemes.

In his master thesis \textcite{hoff2018securecached} developed a distributed in-memory key-value store for caching sensitive data built on top of the Diggi runtime. 
The prototype achieved practical performance, demonstrating the applicability of Diggi.
%A conference paper on implementing comprehensive web stack support in Diggi is in submission pending review, authored by ~\cite{brennaetal}. %TODO: will it be?

Part of the dissertation period included a research internship at Microsoft Research Systems Group in Redmond, Washington. 
The internship lead to explicit acknowledgement for contributions made to published work~\cite{shamis2019fast}. 
Additionally, it awarded co-authorship on two US patent~\cite{castro2019clock}\cite{castro2019performing}, and a submitted journal article detailing Multi-Version Concurrency Control, pending review~\cite{shamis2020}.

As part of the development of Diggi, posters introducing core aspects of the design were presented at ACM SIGOPS Symposium on Operating Systems Principles 2017 Shanghai, China, and the ACM SIGOPS 1st Summer School on Advanced Topics in Systems 2018.
Several talks on the foundations of Diggi have been made as part of seminars related to the masters level course \emph{"INF-3203: Advanced Distributed Systems"} at UiT. 
Diggi is moreover part of the official curriculum for the spring semester of 2020.

\section{Summary of Contributions}
%TODO: attribution of usage, cost, by single functions via the vortex model.
This thesis is based on the work presented in~\cite{gjerdrum2016implementing,gjerdrum2017performance,gjerdrum2018performance,Birrell:2018:SEU:3267323.3268954,gjerdrum2019Diggi}.
We map the individual contributions in each publication to the work presented in this thesis, and list the novel concepts.
\subsection{Publication I}
\fullcite{gjerdrum2016implementing}
\newline

This paper introduces the \emph{CSano} architecture, which outlines a privacy-compliant distributed system architecture for curation and processing of privacy-sensitive data gathered from cyber-physical systems. 
CSano introduces the concept of \emph{vaults} for storing gathered data and \acp{cu} for processing data. 
Vaults and \acp{cu} are composed into a distributed processing pipeline where \emph{information-flow} policies, enforced through security labels, ensure that taint does not cross isolation boundaries in a multi-tenant pipeline. 
CSano describes a distributed tamperproof log structure in which all events in the processing pipeline are logged. 
Accountability is achieved through deterrence in view of a possible audit.
An audit process may replay the logging events to determine the correctness of processed data.
The audit process may additionally prove revocation of rights in the event where consent is withdrawn from the system. 
Computational units and vaults are implemented as individual containers, isolated from one another. 
Vaults are implemented as personal mysql databases hosted in separate containers. The cohosting potential for multi-tenant environments is demonstrated empirically, achieving practical cohosting of 70 separate vaults. 

This work introduces the foundation for Diggi, presenting an initial prototype distributed application runtime for protecting privacy-sensitive data. 
It introduces the concept of accountable reproduction of application execution and tenant/user isolation of compute and storage resources. 
However, this work does not protect from a malicious host, and requires a high degree of trust in the cloud infrastructure, an issue addressed in later work.

\subsection{Publication II and III}
\fullcite{gjerdrum2017performance}
\newline

This paper explores the performance implications of hosting software in \ac{sgx} enclaves.
 We evaluate the technology by constructing a set of micro-architectural benchmarking experiments targeting key traits of the \ac{sgx} platform, including context-switching, memory management and deployment.
We analyze the empirical evidence from the experiments in conformance with detailed descriptions of the internals, and devise a set of programming principles for developing efficient software with Intel \ac{sgx}.
\newline

\fullcite{gjerdrum2018performance}
\newline
 
A selection of papers from the conference where subsequently invited to contribute extended versions for the Springer Cloud Computing and Services Science Journal. 
The extended journal version included additional benchmarks on multithreading performance, and principles recommending asynchrony to maximize system utilization.

The Diggi runtime is built from the ground-up to satisfy these principles.


\subsection{Publication IV}
\fullcite{Birrell:2018:SEU:3267323.3268954}
\newline

This work evaluates several architectural layouts for for interposing reference monitoring to enforce use-based privacy. 
By delegating enforcement to authenticated \ac{sgx} enclaves external to the trust domain, distributed systems may reduce data transfers by enforcing data access remotely, local to the host consuming the data. 
Program attestation may enforce fine-grained policies as the attestation evidence is inherently tied to evidence of usage.

The evaluation was implemented in an early incarnation of the Diggi runtime prototype, and the concepts introduced here further influence the design of Diggi.

\subsection{Publication V}
\fullcite{gjerdrum2019Diggi}
\newline

This work introduces Diggi, a distributed cloud function runtime for hosting native privacy-sensitive applications in an untrusted \ac{faas} cloud.
Functions in Diggi are developed through an asynchronous flow-based programming abstraction. 
This abstraction enables efficient usage of limited SGX resources, maximizing the cohosting potential per server instance of Diggi. 
Diggi implements co-attestation of cloud function to implement distributed trusted architectures, consisting of hundreds of cloud functions.
We evaluate the efficiency of Diggi and demonstrate that our solution is practical, reducing the TCB compared to previous work.
To the best of our knowledge, Diggi is the first asynchronous native FaaS runtime for hosting trusted cloud functions using TEEs.

This work presents the core runtime and security properties of the Diggi prototype, including secure ephemeral storage, co-attestation, asynchronous execution, and communication via tasks and flows.

\subsection{Novel Concepts}
Founded in the publications listed above and additional research, a we summarize the core novel concepts introduced in this thesis:
\paragraph{Persistent cloud functions:}
To broaden the spectrum of applications which may be implemented via \emph{cloud functions}, we introduce persistence. Each function may securely manage ephemeral session state, available throughout its lifetime. A function may additionally be \emph{sticky}, and implement multi-request session interaction, beneficial for streaming workflows.
\paragraph{Accountable function execution:}
We introduce a message-based dynamic attestation process for identifying enclave software beyond initial attestation.
By storing message exchanges in a tamperproof logging structure, functions in Diggi are accountable.
This property further extends to non-deterministic functions. 
Execution may be audited by replaying messages and comparing the expected result to the dynamic attestation proof.
\paragraph{Shielded native cloud function execution runtime:}
We design and implement an efficient, native and fully asynchronous shielded runtime for cloud function execution.
The runtime adopts best practices for shielded software in \ac{sgx}, including low memory footprint, exit-less communication and careful partitioning of program logic.
\paragraph{Protocol for multiparty co-attestation:}
We extend the Intel-provided stock attestation protocol and introduce a multiparty secure attestation and key distribution protocol for distributed applications. 
A cloud function may identify and authenticate several other cloud functions in a non-trusted cloud.

\section{Outline}	
This thesis is structured as follows: 
\paragraph{Chapter 2} studies serverless systems, including challenges and opportunities for serverless computing in an untrusted cloud. 
\paragraph{Chapter 3} describe several Trusted Execution Environments and other hardware-based trusted computing systems, with particular emphasis on \ac{sgx}.

\paragraph{Chapter 4} conducts a baseline experimental analysis on the micro-architectural features of the \ac{sgx} \ac{tee}.  
Based on the outcome, we derive 7 \emph{performance principles} for designing efficient \ac{sgx}-capable software. 
We then derive a set of functional and non-functional requirements for which a secure and efficient serverless cloud application runtime must satisfy. Based on these requirements, we introduce the design of Diggi.
\paragraph{Chapter 5 and 6} details the Diggi prototype implementation, securing serverless applications from an untrusted cloud. 
The prototype implementation includes a trusted asynchronous micro runtime, ephemeral state protection, record-and-replay accountability, system call translation for legacy libraries, cloud function deployment and co-attestation.
\paragraph{Chapter 7} evaluates the prototype implementation, and demonstrates the practicality of the system design through analysis and discussion of the empirical results. 
To demonstrate applicability, we introduce a sample application implemented as as a collection of Diggi cloud functions. 
\paragraph{Chapter 8} discusses Diggi in the context of relevant and topically similar related work. 
\paragraph{Chapter 9} presents concluding remarks, discussing contributions, along with opportunities for future research.