This thesis presents Diggi; a trusted runtime system for implementing secure accountable serverless applications on top of an inherently untrusted public cloud.
Our initial thesis aims to test the following conjecture:
\newline
\begin{center}
\textit{\textbf{\acp{tee} can be leveraged to build a secure and efficient serverless application runtime for trusted computing in a public cloud.}}
\end{center}
\mbox{}
\newline

The security model presented in Section \ref{section:securitymodel}, the opportunities and challenges for serverless systems presented in \ref{section:serverlesschallenges}, and the performance principles derived in Section \ref{section:principles} resulted in a set of requirements for the design of a trusted efficient and accountable serverless runtime. 
Based on these requirements, we subsequently design, implement and evaluate a prototype of the Diggi runtime.

We conclude this thesis by discussing the experimental results from the previous section and seek to answer the research questions stated conforming to the design requirements and our initial conjecture. 
Additionally, we address some of the limitations of Diggi and opportunities for further research.

\section{Conclusion}
In the previous chapter we initially stated four research questions which our experimental evaluation seeks to answer.
Moreover, the design and implementation of Diggi is based on a set of functional and non-functional requirements stated in section \ref{section:requirements}.

We first list each \emph{research question} individually and argue that the evaluation has provided sufficient evidence to corroborate an affirmative answer. 
\paragraph{Research Question 1:} Section \ref{section:legacyexp} illustrates the ability for the Diggi runtime implementation to host legacy libraries without modification in the trusted runtime. Applications must implement translators of system interaction befitting their application, and we demonstrate that doing so for an example library illustrating that a high degree of system interaction is plausible and practical. Additionally, Section \ref{section:mlpipeline} illustrates an end-to-end use case demonstrating that rich applications may be securely developed on top of the Diggi runtime. The MNIST handwritten digit dataset illustrates a real-world use case implementing secure and trusted distributed inference pipeline on an untrusted underlying infrastructure.  
\paragraph{Research Question 2:} All experiments compare the result of Diggi performance against a baseline. Communication overhead adds 9 percent latency overhead compared to the iperf baseline. System call translations add between 5-10x of overhead compared to direct interaction with the system call interface, and message logging adds 42 percent to benchmark runtime to achieve accountable cloud functions. The relative overhead of Diggi is in tune with similar work~\cite{arnautov2016scone}, and the MINIST dataset and TPC-C benchmark demonstrate the application of pseudo-real workloads. Regardless of these results, any applied use of Diggi should carefully analyze the tradeoff between security and performance.
\paragraph{Research Question 3:} Section \ref{section:cohostperf} demonstrates the potential for cohosting mutually distrusting could functions in distinct enclaves. Despite multiplexing physical hardware resources, 24 echo server cloud functions are able to communicate across the network before the load function experiences any significant latency overhead. Section \ref{section:coldstart} demonstrates that the responsiveness of deploying cloud function as the number of concurrent functions increase, achieves an acceptable average latency of 150 ms for 100 cohosted cloud functions. The echo function demonstrates the simplest logic for a Diggi cloud function, consuming roughly 2.2MB of memory. Ephemeral storage ensures that this figure does not grow significantly nor impact the cohosting potential. Each cloud function may explicitly manage application state securely in untrusted memory.
\paragraph{Research Question 4:} Section \ref{section:accontexp} demonstrates the overhead of recording all state mutations for a cloud function into a tamperproof encrypted message log. Compared to the baseline, the impact on performance is significant. Alternatively, the cloud function may store a dynamic proof of execution, which does not impact performance significantly. However, the persisted evidence may then only be used to verify deterministic cloud functions.

\paragraph{Non-Functional Requirements}
Additionally, the experiments demonstrate that the following non-functional requirements are met:
\begin{itemize}
	\item \textbf{Practicality}: We demonstrate that existing systems and full-fledged distributed serverless applications are hostable in Diggi.
	\item \textbf{Granularity}: Composite applications may define the appropriate distributed security context through attestation groups.
	\item \textbf{Efficiency}: Exit-less system call translations demonstrate an increase in performance compared to conventional ocalls. Moreover, reduced memory consumption through Diggi ephemeral storage increases the cohosting potential.
	\item \textbf{Reducing Trust}: Bespoke system interaction through explicitly implemented translator/server pairs reduces the need for broadly implemented system compatibility which would increase the attack surface and \ac{tcb}.
\end{itemize}

\paragraph{Functional Requirements}
Argumentatively, the design and implementation of Diggi satisfies the following functional requirements:
\begin{itemize}
	\item \textbf{Shielded}: Intel \ac{sgx} is used to develop a confidential and integrity protected runtime capable of hosting trusted computations and data in an untrusted environment. The Diggi runtime and ephemeral storage extends the \ac{sgx} security model to implement shielded data storage  without impacting cohosting potential.
	\item \textbf{Authentication}: Collections of Diggi cloud functions may be jointly authenticated and identified through software attestation. We extend the Intel provided software attestation protocol for multiparty attestation, to support \emph{attestation groups}.
	\item \textbf{Revocation}: All information in the Diggi runtime, including storage, is encrypted using ephemeral keys derived from the \ac{sgx} platform. In the presence of hardware failure, these keys are discarded by the \ac{sgx} implementation, rendering persisted state useless. Additionally, given a known attested trusted runtime, we may remotely attest the ability for the system to use these ephemeral keys correctly and guarantee revocation.
	\item \textbf{Accountability}: By recording all inbound and outbound messages we capture the set of state mutations which define a given cloud function execution. All interactions with the external system, including temporal or random events are recorded through the message interface. A reference implementation is able to decrypt and replay the interactions, and verifiably decide if the cloud function executed correctly.
	\item \textbf{Persistence}: Streaming services and reactive real-time analytics processing services are examples of a class of systems which require a weaker form of persist-able state; defined as \emph{ephemeral state}. Diggi ephemeral storage, together with persistent message endpoints which enable repeated interaction between unique cloud function instances, implement persistent cloud functions.
\end{itemize}

We conclude that the initial thesis conjecture may be confirmed, based on the outcome of the experimental evaluation and satisfaction of the functional and non-functional requirements.

\textit{\textbf{\acp{tee} may be leveraged to build a secure and efficient serverless application runtime for trusted computing in a public cloud.}}

Through the Diggi runtime, applications are protected from a potentially malicious service environment, all while maintaining acceptable performance.
Additionally, distributed applications may identify themselves remotely through software co-attestation, and be held accountable through verifiable execution.
The code implementing the Diggi prototype runtime is available as open source on GitHub\footnote{github.com/andersgjerdrum/diggi}.

\section{Future Work}
Serverless computing is at its core defined by precision in attribution, holding a linear relationship between consumption and cost. 
Attribution of usage in a cloud-driven revenue model, requires precisely measuring resource units.
Diggi implements system services, such as memory, storage and compute through message-passing, meaning attribution of cost may be bound to delivery of messages to the cloud function rather than a binary metric.

Host infrastructure may differentiate usage based on the resource consumed, scaling out hardware components individually similar to \textcite{kvalnes2014omni}.
For persistent cloud functions, attributing cost per invocation will underestimate computational expenses, because of the potential longevity.
Multiple models for logging attribution require joint trust in the surrounding system. 
Intuitively, it is the cloud providers responsibility to correctly attribute cost. 
The consumer must then trust that resource usage is not over-stipulated.
Inversely, if the consumer is responsible for measurement, more granularity may be possible by subdividing recorded consumption, shielded from the untrusted cloud provider. 
An \emph{attribution arbiter}, which both cloud and consumer agree is trustworthy by attestation, may act on behalf of both to record usage correctly. This concept is similar to the work presented by \textcite{goltzsche2019acctee}.

Diggi implements a runtime prototype component of a traditional serverless system. 
A fully realized persistent cloud function framework requires managing non-idempotency in execution. 
Conventional systems use reliable message queues to implement at-least-once semantics, however Diggi persistent cloud functions may have side-effects which require exactly-once execution.

Since the work on this thesis began, Intel has released a modified remote attestation service, enabling developer provided attestation evidence. 
Additionally, \emph{Open Enclaves} attempt to create an open standard for enclave development creating a hardware agnostic platform for developing trusted applications. 
Moreover, cloud providers are beginning to support several types of trusted computing services~\cite{trustedazure}. 
Future work for Diggi involves supporting multiple types of trusted hardware, decoupling the dependency on a particular \ac{tee} and associated attestation services.

Diggi ephemeral storage is built on the realization that memory management in \ac{sgx} is costly. 
With the release of dynamic memory management for SGXV2, enclaves may scale memory usage beyond the initial reserved. 
We still expect an overhead associated with enclave memory operations, however the impact brought on by this new memory model is unknown.
Additionally, enclaves which are able to add \ac{epc} pages at runtime must be accounted for in a revisited state mutation recorder.

In the event of an audit, accountable cloud functions provide evidence to the auditor, which replays the account against a reference implementation.
However,  this does not solve non-repudiation in the event of an unavailable system. Future work should explore how non-repudiation can be achieved by logging results to a BFT ledger.

Operating systems, firmware, and all multi-user software such as databases, web-servers and message queues are subject to interference by the host system. 
Both software and hardware construct share caches, registries, pipelines, ALU and peripheral storage bus interfaces, to name a few. 
\textcite{hunt2019isolation} discuss the challenges of a shared system architecture in pursuit of performance versus non-interference. 
Future work should investigate a reference design for solving trusted shared-nothing runtimes in an untrusted cloud.

For resource isolation, modern operating systems are capable of isolating resources with high precision using features such as c~groups or process groups, additionally used in container technology.
In its current form, Diggi is unable to implement fairness among multiple cohosted cloud functions once threading resources are oversubscribed.
An architecture which allow enclave-managed interrupt processing may enable fair scheduling of resources among mutually distrusting participants.

\acp{asic} such as GPU, TPU and FPGAs demonstrate a considerable advantages for highly data-parallel workloads such as convolutional neural networks, compared to conventional \ac{cpu} architectures (SISD). 
A high-performance data-parallel trusted processing system should investigate how to protect analysis of data by multiple mutually distrusting applications on shared \ac{asic}-hardware.

The diggi serverless runtime is realized through a limited prototype implementation which demonstrate selective applicability for hosting sensitive data in an untrusted cloud.
Future research should evaluate the broader applicability of Diggi for developing trusted distributed systems. 


